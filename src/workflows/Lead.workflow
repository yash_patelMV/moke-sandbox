<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Alert_When_Lead_is_created</fullName>
        <description>Email Alert When Lead is created</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Leads_Email</template>
    </alerts>
    <alerts>
        <fullName>Lead_Creation_Email</fullName>
        <ccEmails>poolelotus_moke@westover-autogroup.net</ccEmails>
        <description>Lead Creation Email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Lead_Creation_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Lead_Status</fullName>
        <field>Status</field>
        <literalValue>Contacted</literalValue>
        <name>Change Lead Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>lead_status_change</fullName>
        <field>Status</field>
        <literalValue>Two-way dialogue</literalValue>
        <name>lead status change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>When Lead is Created</fullName>
        <actions>
            <name>Email_Alert_When_Lead_is_created</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Lead_Creation_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Change_Lead_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>NOT(ISBLANK(Email))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>Responding_to_your_MOKE_enquiry</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Responding to your MOKE enquiry</subject>
    </tasks>
</Workflow>
