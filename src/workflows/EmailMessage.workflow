<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>Email Sent</fullName>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.ValidatedFromAddress</field>
            <operation>equals</operation>
            <value>Team MOKE &lt;sales@mokeinternational.com&gt;</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>Read</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
