public class MergeFutureclass {
    @future(callout = true)
    public static void updateLead(Set<Id> Leadid){
        List<Lead> ldList = [SELECT Id, Name FROM Lead WHERE Id=:Leadid ];
        System.debug('ldList'+ldList);
        delete ldList;
    }
}