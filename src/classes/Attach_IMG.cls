public class Attach_IMG {
    
    @future(callout=true)
    public static void image(String Url, String Id){
        Http h = new Http(); 
        HttpRequest req = new HttpRequest(); 
        Url = Url.replace(' ', '%20'); 
        req.setEndpoint(Url); 
        req.setMethod('GET'); 
        req.setHeader('Content-Type', 'image/png'); 
        req.setCompressed(true); 
        req.setTimeout(60000); 
        HttpResponse res  = h.send(req); 
        if(res.getStatusCode() == 200){
            blob retFile = res.getBodyAsBlob();
            Attachment attach = new Attachment(); 
            attach.ParentId = Id; 
            attach.Name = 'Customized Moke.png'; 
            attach.Body = retFile; 
            attach.contentType = 'image/png'; 
            insert attach;    
        }
    }
}