public class UpdateLeadStatus {
    
    @InvocableMethod(label='Update Lead to Two Way')
    public static void updateLead(List<EmailMessage> emailMsg){
        Set<Id> eids = new Set<Id>();
        
        for(EmailMessage em : emailMsg){
            eids.add(em.id);
        }
        Set<Id> ids = new Set<Id>();
        for(EmailMessageRelation em : [SELECT Id, EmailMessageId, RelationId, RelationType, RelationAddress, RelationObjectType, CreatedDate, CreatedById, SystemModstamp, IsDeleted FROM EmailMessageRelation Where EmailMessageId IN: eids]){
            if(em.RelationObjectType == 'Lead')    ids.add(em.RelationId);
        }
        List<Lead> lsLst = [Select Id,Name,Status From Lead Where ID IN: ids AND Status = 'Contacted'];
        
        if(lsLst.size() > 0){
            for(Lead ld : lsLst){
                ld.Status = 'Two-way dialogue';
            }
            update lsLst;
        }
    }
}