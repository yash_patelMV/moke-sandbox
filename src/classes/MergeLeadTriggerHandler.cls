public class MergeLeadTriggerHandler {
    
    List<Lead> recordNewList = new List<Lead>();
    List<Lead> recordOldList = new List<Lead>();
    Map<Id, Lead> recordNewMap = new Map<Id, Lead>();
    Map<Id, Lead> recordOldMap = new Map<Id, Lead>();
    Boolean isInsert, isUpdate, isDelete, isUndelete = false;
    public static boolean recursionController = false;
    
    public MergeLeadTriggerHandler(List<Lead> newList, List<Lead> oldList, Map<Id, Lead> newMap, Map<Id, Lead> oldMap, boolean isInsert, boolean isUpdate, Boolean isDelete, Boolean isUndelete) {
        this.recordNewList = newList;
        this.recordOldList = oldList;
        this.recordNewMap = newMap;
        this.recordOldMap = oldMap;
        this.isInsert = isInsert;
        this.isUpdate = isUpdate;
        this.isDelete = isDelete;
        this.isUndelete = isUndelete;
    }
    
    public void BeforeInsertEvent(){
       updatesDealer();
    }
    
    public void BeforeUpdateEvent(){

    }
    
    public void BeforeDeleteEvent(){}
    
    public void AfterInsertEvent(){
        addfile();
        mergeLead();
    }
    
    public void AfterUpdateEvent(){
        addfile();
    }
    
    public void AfterDeleteEvent(){}
    
    public void AfterUndeleteEvent(){}
    
    public void mergeLead(){
        Set<String> emails = new Set<String>();
        Map<String, Lead> masters = new Map<String, Lead>();
        for(Lead ld : recordNewList){
            emails.add(ld.Email);
            
        }
        emails.remove(null);
        
        Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get('Lead').getDescribe().fields.getMap();
        List<String> editableFields = new List<String>();
        for(Schema.SObjectField fieldRef : fields.values()) {
            Schema.DescribeFieldResult fieldResult = fieldRef.getDescribe();
            if(fieldResult.isUpdateable()) {
                editableFields.add(fieldResult.getname());
            }
        }
        
    	String str = ' SELECT ' + String.join(new List<String>(Schema.getGlobalDescribe().get('Lead').getDescribe().fields.getMap().keySet()), ',') + '  FROM Lead WHERE Email =: emails ';
        if(!Test.isRunningTest()){
    	    str +=' AND Id !=: recordNewList ';
        }
    	str +=' AND  IsConverted = false';
        
        List<Lead>LdList =  Database.query(str);
                
        if(LdList.size() > 0){
            for(Lead ld: LdList){
                masters.put(ld.Email, ld);
            }
            List<Lead>ListToBeDelete = new List<Lead>();
            List<Lead>ListToBeUpdate = new List<Lead>();
            
            for(Lead ld : recordNewList){
                if(masters.containsKey(ld.Email)){
                    Lead master = masters.get(ld.Email);   
                    if(ld.How_many_years_have_you_been_trading__c != null){
                        master.Relationship_Type__c = 'Private Client';
                        master.Country_Importance__c = 'TBC';
                        master.HasOptedOutOfEmail = false;
                    }
                    
                    for(String s: editableFields){
                        if(ld.get(s) != master.get(s) && ld.get(s) != null && ld.get(s) != ''){
                            master.put(s, ld.get(s));
                        }
                    }  
                    if(master != null){
                        ListToBeUpdate.add(master);
                        ListToBeDelete.add(ld);   
                    }
                }
                Set<Id>Ids = new Set<Id>();
                
                for(Lead l: ListToBeDelete){
                    Ids.add(l.Id);
                }
                
                update ListToBeUpdate;
                MergeFutureclass.updateLead(Ids);
        	}
        }
    }
    
    public void addfile(){
        for(Lead ld : recordNewList){
            if(recordOldMap != null){
                Lead l = recordOldMap.get(ld.ID);
                if(ld.Image_Url__c != l.Image_Url__c && ld.Image_Url__c != null){
                    Attach_IMG.image(ld.Image_Url__c,ld.Id);
                }
            }else{
                if(ld.Image_Url__c != null){
                    Attach_IMG.image(ld.Image_Url__c,ld.Id);
                }
            }
        }
    }

    public void updatesDealer(){
        for(Lead ld : recordNewList){
             if(ld.How_many_years_have_you_been_trading__c != null){
                        ld.Relationship_Type__c = 'Private Client';
                        ld.Country_Importance__c = 'TBC';
                        ld.HasOptedOutOfEmail = false;
             }else{
             	ld.HasOptedOutOfEmail = false;
                if(ld.Country_Importance__c == null){
                     ld.Country_Importance__c = 'N/A';
                 }
             
             }
        }
    }
}